'''
    Djunk
    Copyright (C) 2022  Carl Johnson IV (The Ghost of Christmas Never)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from __future__ import print_function

from collections import Mapping, Iterable
import sys

from django.conf import settings
from django.conf.urls import url
from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import RegexURLPattern, RegexURLResolver, reverse


class AutoSiteMap(Sitemap):
    changefreq = 'daily' ###TODO: dynamically determine
    _items = []

    def items(self):
        ###MAYBE: do dynamic URLs as iterators to defer execution nicely (might need caching to play nice w/ lib)

        def inner():
            for pattern,query in self._items:
                if query is None:
                    yield pattern, {}
                else:
                    results = getattr(query, 'values', lambda: query)()

                    if callable(results):
                        results = results()

                    for record in results:
                        yield pattern, record

        return list(inner())

    def location(self, item):
        pattern, record = item

        if isinstance(record, Mapping):
            return reverse(pattern.name, kwargs=record)
        elif isinstance(record, Iterable) and not isinstance(record, basestring):
            return reverse(pattern.name, args=record)
        else:
            return reverse(pattern.name, args=[record])

    @classmethod
    def register(cls, url_pattern, query):
        cls._items.append((url_pattern, query))

    @classmethod
    def sitemapify_url(cls, url_pattern, private=False, query=None):
        if not private:
            if isinstance(url_pattern, RegexURLPattern):
                cls.register(url_pattern, query)
            elif isinstance(url_pattern, RegexURLResolver):
                ###FUTURE: handle by creating a sub-sitemap
                print('WARNING: url includes are not supported yet', file=sys.stderr)
            else:
                raise ValueError

        return url_pattern

    @classmethod
    def faux_url(cls, *args, **kwargs):
        private = kwargs.pop('private', False)
        query = kwargs.pop('query', None)

        return cls.sitemapify_url(url(*args, **kwargs), private=private, query=query)

    @classmethod
    def scan_statics(cls, media_url=None, media_dir=None):
        ###TODO: use the image sitemap format (implement as a sub)
        # only for content we generated (not stock photos)

        media_url = media_url or settings.MEDIA_URL
        media_dir = media_dir or settings.MEDIA_ROOT

        pass
